import { useState } from "react";
import Dropdown from "./components/Dropdown";

const items = [
  { label: "item 1", key: "item-1" },
  { label: "item 2", key: "item-2" },
  { label: "item 3", key: "item-3" },
];

function App() {
  const [selected, setSelected] = useState({});

  function handleChange(item) {
    setSelected(item);
  }

  return (
    <Dropdown onChange={handleChange} items={items}>
      <input defaultValue={selected.label} placeholder="select" />
    </Dropdown>
  );
}

export default App;
