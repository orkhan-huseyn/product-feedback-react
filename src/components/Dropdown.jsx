import { useEffect, useRef, useState } from "react";
import styled, { css } from "styled-components";

const DropdownContainer = styled.div`
  position: relative;
`;

const DropdownContent = styled.div`
  position: absolute;
  background-color: #fff;
  box-shadow: 0 10px 40px -7px rgb(55 63 104 / 35%);
  border-radius: 10px;
  top: 30px;
  left: 0;
  display: none;

  ${(props) =>
    props.show &&
    css`
      display: block;
    `}
`;

const DropdownItem = styled.button`
  cursor: pointer;
  color: #647196;
  padding: 12px 24px;
  border: none;
  background-color: #fff;
  display: flex;
  border-bottom: 1px solid rgba(58, 67, 115, 0.15);
`;

function Dropdown({ onChange, children, items }) {
  const dropdownRef = useRef(null);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});

  useEffect(() => {
    function handleOutsideClick(event) {
      if (!dropdownRef.current.contains(event.target)) {
        setDropdownOpen(false);
      }
    }
    document.addEventListener("click", handleOutsideClick);
    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, []);

  function toggleDropdown() {
    setDropdownOpen((open) => !open);
  }

  function handleChange(event, item) {
    event.stopPropagation();
    if (onChange && item.key !== selectedItem.key) onChange(item);
    setDropdownOpen(false);
    setSelectedItem(item);
  }

  return (
    <DropdownContainer
      ref={dropdownRef}
      onClick={toggleDropdown}
      aria-haspopup="true"
    >
      {children}
      <DropdownContent show={dropdownOpen} role="list">
        {items.map((item) => (
          <DropdownItem
            role="listitem"
            key={item.key}
            onClick={(event) => handleChange(event, item)}
          >
            {item.label}
            {item.key === selectedItem.key ? "✓" : ""}
          </DropdownItem>
        ))}
      </DropdownContent>
    </DropdownContainer>
  );
}

export default Dropdown;
